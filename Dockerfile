FROM debian:buster

WORKDIR /var/www

RUN mkdir -p /run/php && \
    apt-get update && apt-get install -y \
    curl \
    nginx \
    supervisor \
    php-fpm php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-mbstring php-mysql \
    libmariadb-dev \
    mariadb-server \
    mariadb-client

RUN mkdir -p /run/mysqld && \
    chown -R mysql:mysql /run/mysqld && \
    rm -rf /var/www/* && \
    curl -O https://wordpress.org/latest.tar.gz && \
    tar -xzf latest.tar.gz && \
    rm latest.tar.gz && \
    mv wordpress/* . && \
    rm -r wordpress && \
    chown -R www-data:www-data /var/www

COPY . /

CMD ["supervisord", "-c", "/etc/supervisord.conf"]
